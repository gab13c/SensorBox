This is a Senior Design Project for EE and CpE students at Florida State University.
Students Grant Brown, Chris Raices, and Michael Peters are developing a SensorBox
which will act as a data center for the Electrical Engineering Campus on Florida
State University. The project is based off a Raspberry Pi 3 Model B and is
integrated with different sensors from Adafruit and Grove. The students are integrating
the system using Java and Cayenne. The students developed a MQTT server on the Cayenne
IoT dashboard to configure widgets to monitor the data being brought in by the sensors.
If needed you can follow Cayenne's website instructions on how to create an MQTT 
server for your Raspberry Pi. This build is scalable for differnt models of Raspberry
Pi including Model A and Raspberry Pi 2. 

The students will be developing a privite SQL database server to ensure secruity for the 
devices in the data transfer process between the Raspberry Pi's and the local host
of the database server. 

The project is built to be scalable so it may be integrated into larger 
buildings and servers.

Author: Grant Brown
Co-Author's : Chris Raices, Michael Peters
Mentor: Dr. Reza Arghandeh
School: Florida State University